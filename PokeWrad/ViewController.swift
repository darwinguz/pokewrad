//
//  ViewController.swift
//  PokeWrad
//
//  Created by Darwin Guzmán on 13/6/18.
//  Copyright © 2018 Darwin Guzmán. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    let pokemonManager = PokemonManager()
    
    @IBOutlet weak var pokemonTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let network: Network = Network()
        network.getAllPokemon(){(pokemonArray) in
            self.pokemonManager.itemsPokemon = pokemonArray
            self.pokemonTableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return 8
        return pokemonManager.itemsPokemon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = pokemonManager.itemsPokemon[indexPath.row].name
        return cell
    }
    
    //ir a segue on click item
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toPeekPokemonSegue", sender: self)
    }

    //binding con la variable itemInfoPokemon
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toPeekPokemonSegue" {
            let destination = segue.destination as! PeekPokemonViewController
            let selectedRow = pokemonTableView.indexPathsForSelectedRows![0]
            destination.itemInfoPokemon = (pokemonManager, selectedRow.row)
        }
    }
}

