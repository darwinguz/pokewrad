//
//  Pokemon.swift
//  PokeWrad
//
//  Created by Darwin Guzmán on 13/6/18.
//  Copyright © 2018 Darwin Guzmán. All rights reserved.
//

import Foundation

struct Pokemon:Decodable {
    //tiene q estar igual q en el json
    var name:String
    var weight:Int
    var height:Int
    var sprites:Sprite
}


struct Sprite:Decodable{
    var defaultSprite:String
    
    //para no cambiar el camellcase por una variable se mapea:
    enum CodingKeys:String, CodingKey{
        case defaultSprite = "front_default"
    }
}
