//
//  PeekPokemonViewController.swift
//  PokeWrad
//
//  Created by Darwin Guzmán on 20/6/18.
//  Copyright © 2018 Darwin Guzmán. All rights reserved.
//

import UIKit

class PeekPokemonViewController: UIViewController {
    var itemInfoPokemon:(itemManagerPokemon: PokemonManager,index: Int)?
    @IBOutlet weak var pokemonImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var spritesLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        nameLabel.text = itemInfoPokemon?.itemManagerPokemon.itemsPokemon[(itemInfoPokemon?.index)!].name
        let weightTemp:Int = (itemInfoPokemon?.itemManagerPokemon.itemsPokemon[(itemInfoPokemon?.index)!].weight)!
        let heightTemp:Int = (itemInfoPokemon?.itemManagerPokemon.itemsPokemon[(itemInfoPokemon?.index)!].height)!
        weightLabel.text = String(weightTemp)
        heightLabel.text = String(heightTemp)
        
        let url = itemInfoPokemon?.itemManagerPokemon.itemsPokemon[(itemInfoPokemon?.index)!].sprites.defaultSprite
        let bm = Network()
        bm.getImage(url: url!, completionHandler: { (imageR) in
            DispatchQueue.main.async {
                self.pokemonImageView.image = imageR
            }
        })
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
