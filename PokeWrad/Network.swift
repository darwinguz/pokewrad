//
//  Network.swift
//  PokeWrad
//
//  Created by Darwin Guzmán on 13/6/18.
//  Copyright © 2018 Darwin Guzmán. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

class Network{
    //no es una solucion porque solo espera un solo completion la sol. es DispatchGroup
    //func getAllPokemon(completion:@escaping ([Pokemon])->()){
    func getAllPokemon(completion:@escaping ([Pokemon])->()){
        var pokemonArray:[Pokemon] = []
        let group = DispatchGroup()
        
        for i in 1...8{
            //entra 8 veces
            group.enter()
            Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseJSON { response in
                guard let data = response.data else{
                    print("Error")
                    return
                }
                
                guard let pokemon = try? JSONDecoder().decode(Pokemon.self, from: data) else{
                    print("Error decoding Pokemon")
                    return
                }
                pokemonArray.append(pokemon)
                print(pokemon)
                //sale 8 veces
                group.leave()
            }
        }
        //.main=hilo principal
        group.notify(queue: .main){
            completion(pokemonArray)
        }
    }
    
    func getImage(url:String, completionHandler: @escaping(UIImage)->()){
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                completionHandler(image)
            }
        }
    }
}
